#!/usr/bin/env python3
import time
import random

__author__ = 'u5006960'


class DisjointSet(object):
    """
    Provides static functions for use with disjoint sets.
    Algorithms adapted from section 21.3 of Introduction to Algorithms (Cormen et al)
    """

    class DisjointSetNode(object):
        """
        Data structure representing a single node in a disjoint set forest
        """

        def __init__(self, val):
            self.val = val
            self.parent = self
            self.rank = 0


    @staticmethod
    def makeSet(val):
        """
        Creates a new set with the given value
        """
        return DisjointSet.DisjointSetNode(val)

    @staticmethod
    def union(x, y):
        """
        Joins two nodes into a single tree.
        Implemented with union by rank
        """
        DisjointSet.link(DisjointSet.findSet(x), DisjointSet.findSet(y))

    @staticmethod
    def link(x, y):
        if x.rank > y.rank:
            y.parent = x
        else:
            x.parent = y
            if x.rank == y.rank:
                y.rank += 1

    @staticmethod
    def findSet(x):
        """
        Finds the root of a set
        Implemented with path compression.
        """
        if x != x.parent:
            x.parent = DisjointSet.findSet(x.parent)
        return x.parent


def quicksort(edges):
    """
    Sorts a list of edges using quicksort with no specialised pivot selection
    Algorithm adapted from section 7.1 of Introduction to Algorithms (Cormen et al)
    """

    def partition(p, r):
        """
        Partition the subarray from p to r
        """
        pivot = edges[r][2] #Select the final element as a pivot
        i = p - 1
        for j in range(p, r):
            if edges[j][2] <= pivot:
                i += 1
                tmp = edges[i]
                edges[i] = edges[j]
                edges[j] = tmp

        tmp = edges[i + 1]
        edges[i + 1] = edges[r]
        edges[r] = tmp

        return i + 1

    def doQuicksort(p, r):
        if p < r:
            q = partition(p, r)
            doQuicksort(p, q - 1)
            doQuicksort(q + 1, r)

    doQuicksort(0, len(edges) - 1)


def kruskalMST(G, n):
    """
    Creates a minimum spanning tree for G, using Kruskal's algorithm
    """
    A = []
    nodes = []  # Maintain a list of all DisjointSetNodes
    for v in range(n):
        nodes.append(DisjointSet.makeSet(v))

    # Create and sort list of edges
    edges = []
    for u in range(n):
        for v in range(u, n):
            edges.append((u, v, G[u][v]))

    quicksort(edges)

    for edge in edges:
        u = edge[0]
        v = edge[1]
        if DisjointSet.findSet(nodes[u]) != DisjointSet.findSet(nodes[v]):
            A.append((u, v))
            DisjointSet.union(nodes[u], nodes[v])

    return A


def randomGraph(n):
    """
    Generates a random complete undirected graph of size n.
    Note that python's random() function returns a double
    in [0, 1)
    """
    graph = [[0 for i in range(n)] for j in range(n)]
    for u in range(n):
        for v in range(u, n):
            weight = random.random()
            graph[u][v] = weight
            graph[v][u] = weight

    return graph


def testKruskal(n):
    """
    Generates a random graph and then times how long it takes
    to find an MST for that graph.
    """
    random.seed()
    G = randomGraph(n)
    start = time.time()
    mst = kruskalMST(G, n)
    end = time.time()

    L = sum([G[u][v] for (u, v) in mst])

    return L, (end - start)


def main():
    values = [10, 100, 150, 200]
    print("n, Average L(n), Average time (s)")
    for n in values:
        L_total = 0
        time_total = 0
        for i in range(50):
            L, time = testKruskal(n)
            L_total += L
            time_total += time

        print("{},{:.4f},{:.5f}".format(n,
                                        L_total / 50,
                                        time_total / 50))


if __name__ == "__main__":
    main()
