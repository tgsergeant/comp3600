\documentclass{article}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{listings}
\usepackage{color}
\usepackage{hyperref}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
}
\title{COMP3600: Assignment 2}
\date{25 October, 2013}
\author{Timothy Sergeant (U5006960)}

\setcounter{secnumdepth}{0}

\begin{document}

\maketitle

\section{Question 1}

\subsection{Source code listing}
Note that mst.py is a Python3 program, and will not run on Python $\le$ 2.7.
\lstinputlisting{mst.py}

\subsection{Sample output}

Output from a typical execution of mst.py is as follows:

\begin{verbatim}
n, Average L(n), Average time (s)
10,1.0959,0.00023
100,1.1722,0.03067
150,1.2031,0.07334
200,1.2142,0.13730
\end{verbatim}

\subsection{Analysis}

The value of $L(n)$ appears to grow as $n$ grows, but at a decreasing rate.
Across multiple runs of \texttt{mst.py}, the value of $L(n)$ for $n=200$ is
frequently approximately equal to or less than the value for $n=150$. This
indicates that the function has almost stopped growing by this point, with
random variation caused by the random number generation. Hence, we can see that
as $n$ grows, $L(n)$'s rate of growth decreases -- that is, $L(n)/n$ decreases.

As additional nodes are added to the graph, the cost of the minimum edge to/from
each node is expected to decrease. Thus, there are two potential reasons for the
decreasing value of $L(n)/n$:
\begin{enumerate}
  \item The expected cost of connecting a new node to the existing MST decreases
    as $n$ increases.
  \item Adding new nodes to the graph increases the likelihood that existing
    edges in the MST can be replaced by cheaper edges touching the new node.
\end{enumerate}

\section{Question 2}

Since communication between vertices is independent, the reliability from $u$ to
$v$ along some path is the product of all vertex-to-vertex (ie, $p(t, s)$
values) along that path. To find the most reliable path from $u$ to $v$, we can
use a modified version of Dijkstra's algorithm which tracks the highest
cumulative reliability to each vertex (ie, the product of $p$ values) rather
than the lowest cumulative distance  (the sum of $p$ values). Dijkstra's
algorithm is suitable for this task, as there are guaranteed to be no negative
edge-weights in the graph. As we want to track the highest probability, we use a
max-priority queue rather than a min-priority queue.

Pseudocode for this algorithm can be seen below. This algorithm will run with
the same time complexity as Dijkstra's algorithm -- $O(V\log V + E \log V)$,
depending on the data structure chosen for the priority queue.

\begin{algorithm}[htpb]
	\caption{Algorithm for finding the most reliable path from $u$ to $v$}
	\label{alg:reliable}
	\begin{algorithmic}
		\Function{Reliable-Path}{$G$, $u$, $v$, $p$}
			\State $ u.r \gets 1$
			\State $u.\pi \gets NIL$
			\For{$v \in V - \{u\}$}
			\State $v.r \gets - \infty$
			\State $v.\pi \gets NIL$
			\EndFor
			\State $Q = V$
			\While{$Q \not= \emptyset$}
			\State{$n \gets $EXTRACT-MAX$(Q)$}
			\If{$n = v$}
				\State \Return $n$
			\EndIf

			\For{$m \in G.Adj[n]$}
			\If{$m.r < n.r \times p(n, m)$}
			\State $m.r \gets n.r \times p(n, m)$
			\State $m.\pi \gets n$
			\EndIf
			\EndFor
			\EndWhile
		\EndFunction

	\end{algorithmic}
\end{algorithm}

\newpage
\section{Question 3}

We wish to find a sequence $\langle c_{i_1}, c_{i_2}, \dots, c_{i_k} \rangle$ such that

\begin{align*}
	R[i_1, i_2]\cdot R[i_2, i_3] \cdot \dots \cdot R[i_k, i_1] &> 1 \\
	\log(R[i_1, i_2]\cdot R[i_2, i_3] \cdot \dots \cdot R[i_k, i_1]) &> \log 1 \\
	\log R[i_1, i_2] + \log R[i_2, i_3] + \dots + \log R[i_k, i_1] &> 0 \\
	L[i_1, i_2] + L[i_2, i_3] + \dots + L[i_k, i_1] &< 0 \text{ where $L[i, j] = -\log R[i, j]$} 
\end{align*}

So, we create a graph with vertices $V = \{ i | 1 \le i \le n\}$ representing the different
currencies and edges given by $L$. Then, we wish to determine whether there is a negative cycle
in this graph. This can be easily achieved with the Bellman-Ford algorithm, which can find negative
cycles in a graph with $O(VE) = O(n^3)$ time.\footnote{\href{http://math.stackexchange.com/questions/94414/an-algorithm-for-arbitrage-in-currency-exchange}{http://math.stackexchange.com/questions/94414/an-algorithm-for-arbitrage-in-currency-exchange}, accessed 24/10/13}. Note that as we
construct a complete graph, the choice of source vertex does not matter: any negative-weight cycle
will always be reachable from the source.

In order to display the negative cycle, we trace back from the inconsistent edge that was found by
the Bellman-Ford algorithm, through the subgraph defined by the parent pointers, until we reach a
vertex that has been seen before. In the worst case, this will require $O(V) =
O(n)$
time, touching every vertex in the graph. Thus, the worst-case running time of
the algorithm is the same whether or not we print out the sequence: $O(n^3)$.

Pseudocode for this algorithm can be seen below.

\begin{algorithm}[htbp]
	\caption{Detects whether a successful sequence for arbitrage exists, and prints it if it
	does.}
	\label{alg:arbitrage}
	\begin{algorithmic}
		\Function{Find-Arbitrage}{$R$, $n$}
		\State $G.V \gets \{ i | 1 \le i \le n \}$
		\Comment{Initialise the graph G}
		\State $G.E \gets \emptyset $
		\For{$i = 1 \text{ to } n$}
		\For{$j = 1 \text{ to } n$}
			\State $G.E \gets G.E \cup \{(i, j)\}$
			\State $w(i, j) = - \log R[i, j]$
			\EndFor
		\EndFor
		\State $ s \gets G.V[0]$
		\Comment{Initialise the Bellman-Ford algorithm}
		\State $s.d \gets 0$
		\State $s.\pi \gets NIL$
		\For{$v \in G.V \setminus \{s\}$}
			\State $v.d \gets \infty$
			\State $v.\pi \gets NIL$
		\EndFor
		\For{$i = 1 \text{ to } \lvert V \rvert - 1$}
		\Comment{Perform relaxation as per the B-F algorithm}
			\For{$(u, v) \in G.E$}
				\State Relax(u, v, w)
			\EndFor
		\EndFor
		\For{$(u, v) \in G.E$}
		\Comment{Search for inconsistency and find the cycle}
			\If{$v.d > u.d + w(u, v)$}
				%trace the path back
				\State $current \gets v$
				\State $path = \langle \rangle$
				\While{$current \not \in path$}
					\State $path.append(current)$
					\State $current \gets current.\pi$
				\EndWhile
				\State $Print(path)$
				\State \Return path
			\EndIf
		\EndFor
		\State \Return False
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\end{document}
